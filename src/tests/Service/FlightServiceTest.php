<?php

use App\Service\FlightService;

class AppServiceFlightTest extends TestCase
{
    public function test_service_flight_get()
    {
        $flights = (new FlightService)->getFlights();

        $this->assertEquals(1, $flights[0]->id);
    }
}
