<?php

class AppModelFlightTest extends TestCase
{
    public function test_flight_model_all_reponse_has_requirements()
    {
        $flights = (new App\Models\Flight)->all();
        $this->assertArrayHasKey('flights', $flights);
        $this->assertArrayHasKey('groups', $flights);
        $this->assertArrayHasKey('uniqueId', $flights['groups'][0]);
        $this->assertArrayHasKey('totalPrice', $flights['groups'][0]);
        $this->assertArrayHasKey('outbound', $flights['groups'][0]);
        $this->assertArrayHasKey('inbound', $flights['groups'][0]);
        $this->assertArrayHasKey('totalGroups', $flights);
        $this->assertArrayHasKey('totalFlights', $flights);
        $this->assertArrayHasKey('cheapestPrice', $flights);
        $this->assertArrayHasKey('cheapestGroup', $flights);
    }

    public function test_flight_model_all_total_groups()
    {
        $flights = (new App\Models\Flight)->all();
        $this->assertEquals(10, $flights['totalGroups']);
    }

    public function test_flight_model_all_total_flights()
    {
        $flights = (new App\Models\Flight)->all();
        $this->assertEquals(15, $flights['totalFlights']);
    }

    public function test_flight_model_all_cheapest_price()
    {
        $flights = (new App\Models\Flight)->all();
        $this->assertEquals(200, $flights['cheapestPrice']);
    }

    public function test_flight_model_all_cheapest_group()
    {
        $flights = (new App\Models\Flight)->all();
        $this->assertEquals(0, $flights['cheapestGroup']);
    }

    public function test_flight_model_all_group_hanle_groups()
    {
        $res = <<<RES
[
  {
    "id": 1,
    "fare": "1AF",
    "price": 100,
    "outbound": 1,
    "inbound": 0
  },
  {
    "id": 2,
    "fare": "1AF",
    "price": 200,
    "outbound": 0,
    "inbound": 1
  },
  {
    "id": 3,
    "fare": "1AF",
    "price": 100,
    "outbound": 1,
    "inbound": 0
  },
  {
    "id": 4,
    "fare": "1AF",
    "price": 250,
    "outbound": 0,
    "inbound": 1
  },
  {
    "id": 5,
    "fare": "2DA",
    "price": 500,
    "outbound": 1,
    "inbound": 0
  },
  {
    "id": 6,
    "fare": "2DA",
    "price": 800,
    "outbound": 0,
    "inbound": 1
  }
]
RES;
        $flights = (new App\Models\Flight)::handleGroups(json_decode($res));

        $this->assertEquals(300, $flights[0]["totalPrice"]);
        $this->assertEquals([["id" => 1], ["id" => 3]], $flights[0]["outbound"], 'outbound');
        $this->assertEquals([["id" => 2]], $flights[0]["inbound"], 'inbound');

        $this->assertEquals(350, $flights[1]["totalPrice"]);
        $this->assertEquals([["id" => 1], ["id" => 3]], $flights[1]["outbound"], 'outbound');
        $this->assertEquals([["id" => 4]], $flights[1]["inbound"], 'inbound');

        $this->assertEquals(1300, $flights[2]["totalPrice"]);
        $this->assertEquals([["id" => 5]], $flights[2]["outbound"], 'outbound');
        $this->assertEquals([["id" => 6]], $flights[2]["inbound"], 'inbound');
    }

    /**
     * @param $group_id
     * @dataProvider appModelFlightProvider
     */
    public function test_flight_model_all_group($group_id, $expected)
    {
        $flights = (new App\Models\Flight)->all();
        $group   = $flights['groups'][$group_id];

        $this->assertEquals($group_id, $group["uniqueId"]);
        $this->assertEquals($expected['totalPrice'], $group["totalPrice"], "totalPrice");
        $this->assertEquals($expected['outbound'], $group["outbound"], "outbound");
        $this->assertEquals($expected['inbound'], $group["inbound"], "inbound");
    }

    public function appModelFlightProvider()
    {
        return [
            [0, [
                'totalPrice' => 200,
                'outbound'   => [["id" => 1], ["id" => 2], ["id" => 3]],
                'inbound'    => [["id" => 9], ["id" => 10]]
                ]
            ],
            [1, [
                'totalPrice' => 250,
                'outbound'   => [["id" => 1], ["id" => 2], ["id" => 3]],
                'inbound'    => [["id" => 11]]
                ]
            ],
            [2, [
                'totalPrice' => 250,
                'outbound' => [["id" => 4], ["id" => 5], ["id" => 6]],
                'inbound' => [["id" => 9], ["id" => 10]]
                ]
            ],
            [3, [
                'totalPrice' => 270,
                'outbound' => [["id" => 7]],
                'inbound' => [["id" => 9], ["id" => 10]]
                ]
            ],
            [4, [
                'totalPrice' => 300,
                'outbound' => [["id" => 4], ["id" => 5], ["id" => 6]],
                'inbound' => [["id" => 11]]
                ]
            ],
            [5, [
                'totalPrice' => 300,
                'outbound' => [["id" => 8]],
                'inbound' => [["id" => 9], ["id" => 10]]
            ]
            ],
            [6, [
                'totalPrice' => 320,
                'outbound' => [["id" => 7]],
                'inbound' => [["id" => 11]]
                ]
            ],
            [7, [
                'totalPrice' => 350,
                'outbound' => [["id" => 8]],
                'inbound' => [["id" => 11]]
                ]
            ],
            [8, [
                'totalPrice' => 1500,
                'outbound' => [["id" => 15]],
                'inbound' => [["id" => 12], ["id" => 13]]
                ]
            ],
            [9, [
                'totalPrice' => 1700,
                'outbound' => [["id" => 15]],
                'inbound' => [["id" => 14]]
                ]
            ]
        ];
    }
}
