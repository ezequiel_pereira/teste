<?php

namespace App\Service;

class FlightService
{
    CONST URL = "http://prova.123milhas.net/api/flights";

    public function getFlights()
    {
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, self::URL);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);

        return json_decode($output);
    }
}
