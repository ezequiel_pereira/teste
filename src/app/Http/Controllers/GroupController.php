<?php

namespace App\Http\Controllers;

use App\Models\Flight;

class GroupController extends Controller
{
    /**
     * Retrieve the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function index()
    {
        return json_encode(Flight::all());
    }
}
