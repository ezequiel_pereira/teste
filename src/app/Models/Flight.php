<?php

namespace App\Models;

use App\Service\FlightService;

class Flight
{
    static public function all()
    {
        $flights = (new FlightService)->getFlights();
        $groups  = self::handleGroups($flights);

        return [
            "flights"       => $flights, // retorne aqui os voos consultados na api em prova.123milhas.net
            "groups"        => $groups,
            "totalGroups"   => count($groups), // quantidade total de grupos
            "totalFlights"  => count($flights), // quantidade total de voos únicos
            "cheapestPrice" => $groups[0]['totalPrice'] ?? 0, // preço do grupo mais barato
            "cheapestGroup" => 0 // id único do grupo mais barato
        ];
    }

    static public function handleGroups($flights)
    {
        foreach ($flights as $flight) {
            if ($flight->outbound) {
                $fares[$flight->fare]['outbound']['prices'][] = $flight->price;
                $fares[$flight->fare]['outbound']['ids'][$flight->price][] = ['id' => $flight->id];
            } elseif ($flight->inbound) {
                $fares[$flight->fare]['inbound']['prices'][] =  $flight->price;
                $fares[$flight->fare]['inbound']['ids'][$flight->price][] = ['id' => $flight->id];
            }
        }

        $uniqueId = 0;
        $groups   = [];
        foreach ($fares AS $fare) {
            $unique_outbound = array_unique($fare['outbound']['prices']);
            $unique_inbound  = array_unique($fare['inbound']['prices']);

            foreach ($unique_outbound AS $o) {
                foreach ($unique_inbound AS $i) {
                    array_push($groups, [
                        'uniqueId'   => 0,
                        'totalPrice' => $o + $i,
                        "outbound"   => $fare['outbound']['ids'][$o],
                        "inbound"    => $fare['inbound']['ids'][$i]
                    ]);


                }
            }
        }

        $collection = collect($groups);
        $sorted     = $collection->sortBy('totalPrice');

        $result = [];
        foreach ($sorted AS $g) {
            $g['uniqueId'] = $uniqueId;
            $result[] = $g;
            $uniqueId++;
        }

        return $result;
    }
}
