# flights

# Instruções para download

$ git clone https://gitlab.com/ezequiel_pereira/teste.git flights

$ cd flights

##Instalação

$ docker-compose up --build -d

$ docker exec -it flights /bin/bash

### Executar o PHP composer

root@244d9140c456:/flights# composer install

Após executar o docker-compose o lumen roda na porta 8080 conforme 
a configuração do arquivo nginx.conf.

A única rota existente é http://localhost:8080/flights/group

## Executar testes unitários

root@244d9140c456:/flights# ./vendor/bin/phpunit
